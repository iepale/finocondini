changequote([!,!])dnl
% Fino reference sheet
% Jeremy Theler

This reference sheet is for [Fino](index.html) esyscmd([!git describe | sed 's/-/./'!]). 

~~~
$ fino
esyscmd([!fino!])dnl
$
~~~

Note that Fino works on top of [wasora](/wasora), so you should also check the [wasora reference sheet](/wasora/reference.html) also---not to mention the [wasora RealBook](/wasora/realbook).

# Keywords

esyscmd([!../../wasora/doc/reference.sh parser kw!])

# Variables

esyscmd([!../../wasora/doc/reference.sh init va!])

