Merge "conic_valve.brep";

Characteristic Length{1:10} = 0.25;

Physical Volume("bulk") = {1};
Physical Surface("base") = {1};
Physical Surface("cone") = {3};
Physical Surface("top") = {6};
Physical Surface("other") = {2, 4, 5};
